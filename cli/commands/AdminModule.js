const capitalize = require('lodash.capitalize');
const fs = require('fs');
const utils = require('../utils');

const ejs = require('ejs')

const createEntryComponent = (entryPageFile, moduleName) => {
  const entryPageContent = ejs.render(
    fs.readFileSync('node_modules/@josercl/vue-admin-commands/cli/templates/pageEntry.ejs', 'utf-8'),
    { moduleName, moduleNameCap: capitalize(moduleName) }
  )
  fs.writeFileSync(entryPageFile, entryPageContent)
};

const createListComponent = (listPageFile, moduleName) => {
  const listPageContent = ejs.render(
    fs.readFileSync('node_modules/@josercl/vue-admin-commands/cli/templates/pageList.ejs', 'utf-8'),
    { moduleName, moduleNameCap: capitalize(moduleName) }
  )
  fs.writeFileSync(listPageFile, listPageContent)
};

const createEditComponent = (editPageFile, moduleName) => {
  const pageContent = ejs.render(
    fs.readFileSync('node_modules/@josercl/vue-admin-commands/cli/templates/pageEdit.ejs', 'utf-8'),
    { moduleName, moduleNameCap: capitalize(moduleName) }
  )
  fs.writeFileSync(editPageFile, pageContent)
};

const createViewComponent = (viewPageFile, moduleName) => {
  const pageContent = ejs.render(
    fs.readFileSync('node_modules/@josercl/vue-admin-commands/cli/templates/pageView.ejs', 'utf-8'),
    { moduleName, moduleNameCap: capitalize(moduleName) }
  )
  fs.writeFileSync(viewPageFile, pageContent)
};

const createRoutesFile = (routesFile, moduleName) => {
  const pageContent = ejs.render(
    fs.readFileSync('node_modules/@josercl/vue-admin-commands/cli/templates/router.ejs', 'utf-8'),
    { moduleName, moduleNameCap: capitalize(moduleName) }
  )
  fs.writeFileSync(routesFile, pageContent)
};

const create = ({
  name, url
}) => {
  /*if (createVuex) {
    vuexModule.create({
      name,
      crud: snakeCase(vuexVar).toUpperCase(),
    });
    vuex = name
  }*/

  const destFolder = `src/features/${name}`;

  const STORE_FOLDER = `${destFolder}/store`

  utils.mkdir(destFolder);
  utils.mkdir(`${destFolder}/views`);
  utils.mkdir(STORE_FOLDER);

  const ENTRY_PAGE_FILE = `${destFolder}/views/${name}.vue`;
  const LIST_PAGE_FILE = `${destFolder}/views/${name}List.vue`;
  const EDIT_PAGE_FILE = `${destFolder}/views/${name}Edit.vue`;
  const VIEW_PAGE_FILE = `${destFolder}/views/${name}View.vue`;
  const ROUTES_FILE = `${destFolder}/routes.js`;

  const STORE_FUNCTIONS = `${STORE_FOLDER}/functions.js`
  const STORE_DEF = `${STORE_FOLDER}/index.js`

  createEntryComponent(ENTRY_PAGE_FILE, name);
  createListComponent(LIST_PAGE_FILE, name);
  createEditComponent(EDIT_PAGE_FILE, name);
  createViewComponent(VIEW_PAGE_FILE, name);
  createRoutesFile(ROUTES_FILE, name);

  fs.writeFileSync(
    STORE_FUNCTIONS,
    ejs.render(
      fs.readFileSync('node_modules/@josercl/vue-admin-commands/cli/templates/storeFunctions.ejs', 'utf-8'),
      { moduleUrl: url }
    )
  )

  fs.writeFileSync(
    STORE_DEF,
    ejs.render(
      fs.readFileSync('node_modules/@josercl/vue-admin-commands/cli/templates/storeIndex.ejs', 'utf-8'),
      { moduleName: name, moduleNameCap: capitalize(name) }
    )
  )

  fs.writeFileSync(`${destFolder}/form.js`, 'export default [];');
};

module.exports = {
  create,
};
